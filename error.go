package adverr

import "fmt"

// ErrorDefinition can be used to make new errors of the same kind
type ErrorDefinition struct {
	id             uint64
	definitionData interface{}
	pkgName        string
	message        string
}

// Error satisfies the error interface
type Error struct {
	definition  ErrorDefinition
	errorData   interface{}
	messageData []interface{}
}

// Equals returns true if the given error uses the same ErrorDefinition
func (e *Error) Equals(other error) bool {
	assertedErr, ok := other.(*Error)
	if !ok {
		return false
	}
	return e.definition.id == assertedErr.definition.id
}

// Error formats its message with its data and returns it
func (e *Error) Error() string {
	return fmt.Sprintf(e.definition.pkgName+": "+e.definition.message, e.messageData...)
}

// Message returns only the formatted message (useful for frontend error messages)
func (e *Error) Message() string {
	return fmt.Sprintf(e.definition.message, e.messageData...)
}

// Data returns the error data
func (e *Error) Data() interface{} {
	return e.errorData
}

// DefData returns the definition data of e
func (e *Error) DefData() interface{} {
	return e.definition.definitionData
}

// NewWithData returns a new Error instance with the given error data
func (ed ErrorDefinition) NewWithData(errorData interface{}, messageData ...interface{}) *Error {
	return &Error{
		ed,
		errorData,
		messageData,
	}
}

// New returns a new Error instance with only message data
func (ed ErrorDefinition) New(messageData ...interface{}) *Error {
	return &Error{
		ed,
		nil,
		messageData,
	}
}

// Data returns the definition data
func (ed ErrorDefinition) Data() interface{} {
	return ed.definitionData
}

// Equals returns true if the given error has the same error definition
func (ed ErrorDefinition) Equals(other error) bool {
	if assertedErr, ok := other.(*Error); ok {
		return ed.id == assertedErr.definition.id
	}
	return false
}

// for better readability
type packageName = string
type errorName = string

var registeredErrors = map[packageName]map[errorName]ErrorDefinition{}

var idCounter uint64

// Register registers a new ErrorDefinition
func Register(pkgName, errName, rawMessage string) ErrorDefinition {
	return RegisterWithData(pkgName, errName, rawMessage, nil)
}

// RegisterWithData registers a new ErrorDefinition with definition data
func RegisterWithData(pkgName, errName, rawMessage string, data interface{}) ErrorDefinition {
	_, ok := registeredErrors[pkgName]
	if !ok {
		registeredErrors[pkgName] = map[errorName]ErrorDefinition{}
	}
	err := ErrorDefinition{
		id:             newID(),
		definitionData: data,
		pkgName:        pkgName,
		message:        rawMessage,
	}
	registeredErrors[pkgName][errName] = err
	return err
}

func newID() uint64 {
	id := idCounter
	idCounter++
	return id
}

// Get returns the ErrorDefinition which is registered under the given name
func Get(pkgName, errName string) ErrorDefinition {
	errs, ok := registeredErrors[pkgName]
	if !ok {
		return errorNotFoundErr
	}
	err, ok2 := errs[errName]
	if !ok2 {
		return errorNotFoundErr
	}
	return err
}

// Err is a combined call of adverr.Get(...).New(...)
func Err(pkgName, errName string, messageData ...interface{}) *Error {
	return ErrWithData(pkgName, errName, nil, messageData)
}

// ErrWithData is a combined call of adverr.Get(...).NewWithData(...)
func ErrWithData(pkgName, errName string, errorData interface{}, messageData ...interface{}) *Error {
	return Get(pkgName, errName).NewWithData(errorData, messageData)
}

var errorNotFoundErr = Register("adverr", "ErrorNotFound", "error could not be found")
