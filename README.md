# How to use

1. Import this package: `import "adverr"`
1. Register your error types under your package name: `adverr.Register("main", "UserNotFoundErr", "The user %s could not be found")`
1. Create a new error instance of a registered error: `return nil, adverr.Get("main", "UserNotFoundErr").New(userName)`